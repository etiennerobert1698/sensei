import {Deserializable} from './Deserializable';

export class Skill implements Deserializable {
    id: number;
    name: string;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}

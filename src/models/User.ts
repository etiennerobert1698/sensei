import {Deserializable} from './Deserializable';

export class User implements Deserializable {
    id: number;
    username: string;
    mail: string;
    password: string;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
}

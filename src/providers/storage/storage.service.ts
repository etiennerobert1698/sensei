import {Injectable} from '@angular/core';
import {User} from '../../models/User';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    auth = new User();
    token: '';

    constructor() {
    }

    isAuth() {
        return !!this.auth.id;
    }
}

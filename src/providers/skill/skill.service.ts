import {Injectable} from '@angular/core';
import {StorageService} from '../storage/storage.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SkillService {

    constructor(private storageService: StorageService, private http: HttpClient) {
    }

    find() {
        return this.http.get(environment.apiUrl + 'skills/');
    }

    findOne(id) {
        return this.http.get(environment.apiUrl + 'skills/' + id);
    }
}

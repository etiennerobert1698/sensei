import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../providers/auth/auth.service';
import {StorageService} from '../../providers/storage/storage.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-connection',
    templateUrl: './connection.page.html',
    styleUrls: ['./connection.page.scss'],
})
export class ConnectionPage implements OnInit {

    email: string;
    password: string;

    constructor(private authService: AuthService, private storageService: StorageService, private router: Router) {

    }

    async login() {
        this.authService.login(this.email, this.password).subscribe((data: any) => {
            if (data.user && data.access_token) {
                this.storageService.auth.deserialize(data.user);
                this.storageService.token = data.access_token;
                this.router.navigateByUrl('/tabs/home');
            }
        }, (err) => {
            console.log(err);
        });
    }

    ngOnInit() {
        this.email = 'forfert.jerome@outlook.fr';
        this.password = 'motdepasse';
    }

}

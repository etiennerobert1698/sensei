import {Component, OnInit} from '@angular/core';
import {SkillService} from '../../providers/skill/skill.service';
import {Router} from '@angular/router';
import {Skill} from '../../models/Skill';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

    skills: Array<Skill>;

    constructor(private skillService: SkillService, private router: Router) {
    }

    ngOnInit() {
        this.skillService.find().subscribe((data: []) => {
            this.skills = data.map(obj => (new Skill()).deserialize(obj));
        }, (err) => {
            console.log(err);
        });
    }

    goToSkillPage(id) {
        this.router.navigateByUrl('/tabs/skill/' + id);
    }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import {AuthGuard} from '../../guards/auth/auth.guard';
import {UnauthGuard} from '../../guards/unauth/unauth.guard';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'connection',
        canActivate: [UnauthGuard],
        children: [
          {
            path: '',
            loadChildren: () =>
                import('../connection/connection.module').then(m => m.ConnectionPageModule)
          }
        ]
      },
      {
        path: 'register',
        canActivate: [UnauthGuard],
        children: [
          {
            path: '',
            loadChildren: () =>
                import('../register/register.module').then(m => m.RegisterPageModule)
          }
        ]
      },
      {
        path: 'home',
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            loadChildren: () =>
                import('../home/home.module').then(m => m.HomePageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/connection',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/connection',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}

import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../providers/auth/auth.service';
import {StorageService} from '../../providers/storage/storage.service';
import {User} from '../../models/User';

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

    user = new User();

    constructor(private authService: AuthService, private storageService: StorageService) {
    }

    ngOnInit() {
    }

    register() {
        this.authService.register(this.user).subscribe((data: any) => {
            if (data.user && data.jwt) {
                this.storageService.auth.deserialize(data.user);
                this.storageService.token = data.jwt;
            }
        }, (err) => {
            console.log(err);
        });
    }
}
